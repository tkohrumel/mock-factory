class OpportunityContactRole < ActiveRecord::Base
  attr_accessible :ContactId, :IsPrimary, :OpportunityId

  # {sf => local}
  def self.foreign_key_map
  	{
  		"ContactId" => "ContactId",
  		"Id" => "OpportunityId"
  	}
  end
end
