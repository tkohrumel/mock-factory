require 'faker'

class Account < ActiveRecord::Base
  @@salesforce_required_fields = %w(name)

  attr_accessible :name, :sfdc_id

  before_create :init

  def self.salesforce_required_fields
  	@@salesforce_required_fields
  end

  def self.foreign_key_map
  	{}
  end

  def init
  	self.Name = Faker::Company.name
  end
end