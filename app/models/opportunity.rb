class Opportunity < ActiveRecord::Base
  belongs_to :contact
  attr_accessible :Amount, :ClosedDate, :LeadSource, :Name, :sfdc_id, :StageName, :ContactId, :AccountId

  before_create :init


  @@salesforce_required_fields = %w(AccountId, ContactId)

  def init
    stages = ['Prospecting', 'Qualification', 'Needs Analysis', 'Value Proposition', 'Id. Decision Makers', 'Perception Analysis', 'Proposal/Price Quote', 'Negotiation/Review', 'Closed Won', 'Closed Lost']
    sources = ['External Referral', 'Web', 'Phone Inquiry', 'Purchased List']	

    self.Name = "Opty-" + rand(10..200).to_s
    self.Amount = [50000, 100000, 500000].sample
    self.StageName = stages.sample
    self.LeadSource = sources.sample
    self.CloseDate = rand_time(2.days.ago)
  end

  # {"salesforce_column_name_in_csv" => "local_db_column"}
  def self.foreign_key_map
    {
      "Id" => "ContactId",
      "AccountId" => "AccountId"
    }
  end

  private

  def rand_time(from, to=Time.now)
    Time.at(rand_in_range(from.to_f, to.to_f))
  end

  def rand_in_range(from, to)
    rand * (to - from) + from
  end
end
