require 'faker'

class Contact < ActiveRecord::Base
  belongs_to :account
  has_many :opportunities

  @@salesforce_required_fields = %w(AccountId LastName)

  attr_accessible :AccountId, :LastName, :sfdc_id

  before_create :init

  def self.salesforce_required_fields
  	@@salesforce_required_fields
  end

  # {"salesforce_column_name_in_csv" => "local_db_column"}
  def self.foreign_key_map
  	{
  		"ID" => "AccountId"
  	}
  end

  def init
  	self.LastName = Faker::Name.last_name
  end
end
