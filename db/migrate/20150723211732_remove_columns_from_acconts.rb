class RemoveColumnsFromAcconts < ActiveRecord::Migration
  def change
    change_table :accounts do |t|
      t.remove :parent_id
      t.remove :grandparent_id
    end
  end
end