class AddPrimaryToOpportunityContactRoles < ActiveRecord::Migration
  def change
    add_column :opportunity_contact_roles, :primary, :string
  end
end
