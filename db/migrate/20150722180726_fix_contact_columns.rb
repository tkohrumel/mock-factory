class FixContactColumns < ActiveRecord::Migration
  def change
    change_table :contacts do |t|
      t.rename :last_name, :LastName
      t.rename :parent_id, :AccountId
    end
  end
end