class CreateOpportunityContactRoles < ActiveRecord::Migration
  def change
    create_table :opportunity_contact_roles do |t|
      t.string :contact_id
      t.string :opportunity_id
      t.boolean :is_primary

      t.timestamps
    end
  end
end
