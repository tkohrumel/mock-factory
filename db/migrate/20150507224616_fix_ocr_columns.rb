class FixOcrColumns < ActiveRecord::Migration
  def up
    change_column :opportunity_contact_roles, :is_primary, :string
  end

  def down
    change_column :opportunity_contact_roles, :is_primary, :boolean
  end
end