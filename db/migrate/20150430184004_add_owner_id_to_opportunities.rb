class AddOwnerIdToOpportunities < ActiveRecord::Migration
  def change
    add_column :opportunities, :owner_id, :string
  end
end
