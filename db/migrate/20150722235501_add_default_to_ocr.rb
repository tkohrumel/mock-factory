class AddDefaultToOcr < ActiveRecord::Migration
  def change
    change_column :opportunity_contact_roles, :IsPrimary, :string, :default => "true"
  end
end