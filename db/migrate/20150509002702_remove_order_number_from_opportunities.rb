class RemoveOrderNumberFromOpportunities < ActiveRecord::Migration
  def up
    remove_column :opportunities, :owner_id, :order_number
  end

  def down
    add_column :opportunities, :owner_id, :string
    add_column :opportunities, :order_number, :string
  end
end