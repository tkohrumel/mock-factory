class AdjustAcctColumn < ActiveRecord::Migration
  def change 
    change_table :accounts do |t|
      t.rename :name, :Name
    end
  end
end