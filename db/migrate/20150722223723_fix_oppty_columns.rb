class FixOpptyColumns < ActiveRecord::Migration
  def change
    change_table :opportunities do |t|
      t.remove :parent_id
      t.remove :grandparent_id
      t.rename :account_id, :AccountId
      t.rename :contact_id, :ContactId
      t.rename :closed_on, :CloseDate
      t.rename :lead_source, :LeadSource
      t.rename :stage, :StageName
      t.rename :amount, :Amount
    end
  end
end