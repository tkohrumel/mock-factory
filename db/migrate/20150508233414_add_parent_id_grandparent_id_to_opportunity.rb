class AddParentIdGrandparentIdToOpportunity < ActiveRecord::Migration
  def up 
    add_column :opportunities, :parent_id, :string
    add_column :opportunities, :grandparent_id, :string

    add_column :accounts, :parent_id, :string
    add_column :accounts, :grandparent_id, :string

    add_column :contacts, :parent_id, :string
    add_column :contacts, :grandparent_id, :string    
  end

  def down
    remove_column :opportunities, :parent_id, :string
    remove_column :opportunities, :grandparent_id, :string

    remove_column :accounts, :parent_id, :string
    remove_column :accounts, :grandparent_id, :string

    remove_column :contacts, :parent_id, :string
    remove_column :contacts, :grandparent_id, :string     
  end
end