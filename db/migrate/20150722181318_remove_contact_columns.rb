class RemoveContactColumns < ActiveRecord::Migration
  def change
    change_table :contacts do |t|
      t.remove :account_id
      t.remove :grandparent_id 
    end
  end
end