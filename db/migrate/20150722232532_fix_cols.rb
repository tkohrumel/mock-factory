class FixCols < ActiveRecord::Migration
  def change
    change_table :opportunities do |t|
      t.rename :name, :Name
    end
  end
end