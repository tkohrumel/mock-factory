class FixOcr < ActiveRecord::Migration
  def change
    change_table :opportunity_contact_roles do |t|
      t.rename :contact_id, :ContactId
      t.rename :opportunity_id, :OpportunityId
      t.rename :is_primary, :IsPrimary
    end
  end
end