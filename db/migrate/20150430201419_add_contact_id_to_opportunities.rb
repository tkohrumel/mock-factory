class AddContactIdToOpportunities < ActiveRecord::Migration
  def change
    add_column :opportunities, :contact_id, :string
  end
end
