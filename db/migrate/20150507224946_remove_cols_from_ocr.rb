class RemoveColsFromOcr < ActiveRecord::Migration
  def up
    remove_column :opportunity_contact_roles, :primary
    remove_column :opportunity_contact_roles, :is_primary_string
  end

  def down
    add_column :opportunity_contact_roles, :primary
    add_column :opportunity_contact_roles, :is_primary_string
  end
end