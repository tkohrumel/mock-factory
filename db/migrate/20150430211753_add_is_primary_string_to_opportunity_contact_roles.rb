class AddIsPrimaryStringToOpportunityContactRoles < ActiveRecord::Migration
  def change
    add_column :opportunity_contact_roles, :is_primary_string, :string
  end
end
