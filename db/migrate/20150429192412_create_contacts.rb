class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :sfdc_id
      t.string :account_id
      t.string :last_name
    end
  end
end
