require 'smarter_csv'
require 'benchmark'
require 'salesforce_bulk'

DESKTOP_PATH = ENV["HOME"] + '/Desktop/'

namespace :mock do
  desc "Create mock Account records"
  task :accounts => :environment do
    create_accounts
  end

  desc "Create mock Contact records using Ids from Account records"
  task :contacts => :environment do
    create_contacts  
  end

  desc "Create mock Opportunity records using Ids and AccountIds from Contact records"
  task :opportunities => :environment do
    create_opportunities
  end

  desc "Create mock OpportunityContactRole records using Id and ContactId from Opportunity records"
  task :ocrs => :environment do
    create_ocrs
  end
end

namespace :db do
  desc "List all tables"
  task :describe => :environment do
    ActiveRecord::Base.connection.tables.each do |table|
      next if table.match(/\Aschema_migrations\Z/)
      klass = table.singularize.camelize.constantize      
      puts "#{klass.name} has #{klass.count} records"
    end
  end 

  desc "Clear all tables"
  task :clear_all => :environment do
    clear_tables
  end

  desc "Clear Contacts table"
  task :clear_contacts => :environment do
    clear_contacts
  end

  desc "Clear Opportunities table"
  task :clear_opportunities => :environment do
    clear_opportunities
  end

  desc "Clear OpportunityContactRoles table"
  task :clear_ocrs => :environment do
    clear_ocrs
  end
end

at_exit { wrap_up }

private

# -----------------------
#       create
# -----------------------


def use_bulk
  ENV["bulk"] ? true : false
end

def create_accounts    
  @model = Account
  @skip_columns = %w(parent_id grandparent_id)

  
  puts "How many mock account records do you want to create?"
  num_accounts = $stdin.gets.chomp.to_i

  return unless confirm "are you sure you want to create #{num_accounts} account records?"

  create_records(nil, num_accounts)
  export_records
end

def create_contacts
  @model = Contact

  accounts = get_input
  ratio = get_ratio

  puts
  return unless confirm "create total of #{ratio * accounts.length} contact records (#{ratio} per account)?"

  create_records(accounts, ratio)
  export_records
end

def create_opportunities
  @model = Opportunity
  @skip_columns = %w(account_id contact_id)

  contacts = get_input
  ratio = get_ratio

  return unless confirm "create #{ratio * contacts.length} #{@model.table_name} records (#{ratio} per contact)?"

  create_records(contacts, ratio)
  export_records
end

def create_ocrs
  @model = OpportunityContactRole
  @skip_columns = %w(created_at updated_at)
  opportunities = get_input
  ratio = 1

  return unless confirm "create #{opportunities.length} #{@model.table_name} records?"

  create_records(opportunities, ratio)
  export_records
end

def create_records(associations, ratio)
  total_records = associations ? (associations.count * ratio) : ratio
  progress = ProgressBar.create(title: "Creating Records", total: total_records,  format: "%a %e %P%   #{@model.name.pluralize} Created: %c from %C")
  
  if associations
    ActiveRecord::Base.transaction do 
      associations.each do |a|
        ratio.times do
          assign_keys(@model.new, a).save!
          progress.increment
        end
      end
    end #end transaction
  else  
    ActiveRecord::Base.transaction do
      ratio.times do
        @model.create
        progress.increment
      end
    end
  end
end  

def assign_keys(record, association)
  @model.foreign_key_map.each do |foreign_key, local_column|
    record.send("#{local_column}=", association[foreign_key.downcase.to_sym])
  end
  record
end

def confirm(prompt)
  puts prompt + " [y,N] \n"
  $stdin.gets.chomp.downcase == 'y'
end

# returns array of hashes
def get_input
  puts "Enter the absolute filepath for the csv generated by the Salesforce data loader (e.g. /Users/name/Desktop/success.csv): "
  file = SmarterCSV.process($stdin.gets.chomp, :row_sep => "\n")

  puts
  puts "Loaded successfully", "File has #{file.length} records"
  file
end

def get_ratio
  puts "", "Enter the number of child records per parent record to create: "
  $stdin.gets.chomp.to_i
end

def get_creds
  if ENV["BSFU"] && ENV["BSFPT"]
    {"username" => ENV["BSFU"], "password" => ENV["BSFPT"]}
  else
    puts "Enter your Salesforce username: "
    username = $stdin.gets.chomp
    puts

    puts "Enter your password + security token: "
    password = $stdin.gets.chomp
    puts

    {"username" => username, "password" => password} 
  end
end

def export_records
  use_bulk ? bulk_load : write_table_to_file
end

def bulk_load
  puts "Loading new records to your org via Bulk API:",""
  records_to_insert = []
  creds = get_creds

  bulk_api = SalesforceBulk::Api.new(creds["username"], creds["password"])

  progress = ProgressBar.create(title: "Building Bulk Job", total: @model.count,  format: "%a %e %P%  #{@model.name.pluralize} Added to Job: %c from %C")

  @model.all.each do |record|
    new_record = {}
    @model.salesforce_required_fields.each do |rf|
      new_record.store(rf.to_s, record.send(rf))
    end

    records_to_insert.push(new_record)
    progress.increment
  end

  puts "Creating job...",""

  result = bulk_api.create(@model.name, records_to_insert)

  puts result.result.errors if result.result.errors
  puts "Result: #{result.result.message}", ""

  puts "In your org, go to Setup > Monitoring > Bulk Data Load Jobs to monitor job status", ""
end

def write_table_to_file
  fields = get_relevant_columns

  puts "Writing results to csv file on your desktop. Enter a name for this file (e.g. 'pg_mock_#{@model.name.pluralize.downcase}'): "
  file_dest = DESKTOP_PATH + $stdin.gets.chomp + '.csv'

  begin
    ActiveRecord::Base.connection.execute "copy (select #{fields} from #{@model.table_name}) TO '#{file_dest}' with CSV HEADER"
  rescue => e
    puts "exception writing: ", e
  else
    puts "write done...", "contents of table: #{@model.table_name} (fields: #{fields}) written to #{file_dest}"
  end
end

def get_relevant_columns
  @model.column_names.delete_if { |c| c == "sfdc_id" || c == "id" || (@skip_columns.include?(c) if @skip_columns) }.map {|c| "\"#{c}\""}.join(', ')
end

def wrap_up
  @model = @skip_columns = nil
  puts "", "done" 
end


# --------------------
#       delete
# --------------------


def clear_tables
  return unless confirm 'WARNING: you are about to clear all records. are you sure?'

  puts "clearing all tables in postgres..."

  ActiveRecord::Base.connection.tables.map{|x|x.classify.safe_constantize}.compact.each do |model|
    puts "clearing all #{model.table_name} records"
    model.delete_all
  end
end

def clear_contacts
  return unless confirm 'are you sure you want to clear the contacts table?'
  puts "clearing Contacts table..."
  Contact.delete_all
end

def clear_opportunities
  return unless confirm 'are you sure you want to clear the opportunity table?'

  puts "clearing #{Opportunity.all.length} records from Opportunities table..."
  Opportunity.delete_all    
end

def clear_ocrs
  return unless confirm 'are you sure you want to clear the opportunity_contact_roles table?'

  puts "clearing #{OpportunityContactRole.all.length} records from Opportunities table..."
  OpportunityContactRole.delete_all    
end