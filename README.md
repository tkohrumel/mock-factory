## About
Reference architecture for creating mock data for Salesforce orgs using Ruby on Rails.

## Requirements
* [Ruby](http://code.tutsplus.com/tutorials/how-to-install-ruby-on-a-mac--net-21664)
* [Ruby on Rails](https://gorails.com/setup/osx/10.10-yosemite)
* [PostgreSQL](http://www.postgresql.org/docs/8.0/static/tutorial-createdb.html)

## Configuration

##### Get the repository
To get started, get the latest version of the repo by either downloading the [ZIP](https://github.com/tkohrumel/tk-forcefactory/archive/master.zip) or cloning the contents:  
```bash
git clone https://github.com/tkohrumel/tk-forcefactory.git  
```
Next, change to the directory in which the repository was unzipped or cloned:
```bash
cd tk-forcefactory 
```

##### Set up the database

Once you've set up a Postgres database for this app: 

- 1. create a *[database.yml](https://github.com/tkohrumel/tk-forcefactory/blob/master/config/database.yml.example)* file with the database name, username, and password in the config directory. 
- 2. run migrations

```bash
rake db:migrate
```

##### Install required gems

```bash
bundle install
```

## Getting started

Say we need to create 10000 Account records, then 10 Contacts/Account, and finally 10 Opportunities/Contact. Begin by creating the root records:
```
| => rake mock:accounts
How many mock account records do you want to create?
10000
are you sure you want to create 10000 account records? [y,N]
```
This will create the Account records, populating each field that's required in Salesforce with mock data. Next it will write the records a CSV file to the desktop that will be used to load into your Salesforce org via the [Data Loader](https://help.salesforce.com/apex/HTViewHelpDoc?id=installing_the_data_loader.htm&language=en_US):

```
Time: 00:00:13 Time: 00:00:13 100.00%   Accounts Created: 10000 from 10000
Writing results to csv file on your desktop. Enter a name for this file (e.g. 'pg_mock_accounts'): 
mock_account_records
write done...
contents of table: accounts (fields: "Name") written to /Users/username/Desktop/mock_account_records.csv

done
```
When you use the Data Loader to upload this file, *mock_account_records.csv* into your Salesforce org, it will write a CSV file to your hard drive (e.g. *success072215025253691.csv*) that contains the Salesforce primary keys for the mock Account records we generated. With the Account primary keys handy, we can follow this pattern to generate mock associated Contact records, etc.:
```
| => rake mock:contacts
Enter the absolute filepath for the csv generated by the Salesforce data loader (e.g. /Users/name/Desktop/success.csv): 
/Users/username/Desktop/success072215025253691.csv

Loaded successfully
File has 1000 records

Enter the number of child records per parent record you wish to create: 
10 

create total of 10000 contact records (10 per account)? [y,N] 
y

...
```

## Available Commands
```ruby
rake mock:accounts           # Create mock Account records
rake mock:contacts           # Create mock Contact records using Ids from Account records
rake mock:ocrs               # Create mock OpportunityContactRole records using Id and ContactId from Opportunity records
rake mock:opportunities      # Create mock Opportunity records using Ids and AccountIds from Contact records

rake db:clear_all            # Clear all tables
rake db:clear_contacts       # Clear Contacts table
rake db:clear_ocrs           # Clear OpportunityContactRoles table
rake db:clear_opportunities  # Clear Opportunities table
```

## Additional Resources
- [Wiki](https://github.com/tkohrumel/tk-forcefactory/wiki)
- [Generating and Loading Representative Test Data for Salesforce and Force.com Orgs](http://wiki.developerforce.com/page/Generating_and_Loading_Representative_Test_Data_for_Salesforce_and_Force.com_Orgs).


## License
Copyright (c) 2011, salesforce.com, Inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
    * Neither the name of the salesforce.com, Inc. nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.